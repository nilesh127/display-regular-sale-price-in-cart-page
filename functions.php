<?php

// ==============================================================================
//	Display Regular and sale price in cart page.
// ============================================================================== 

add_filter( 'woocommerce_cart_item_price', 'nutrabox_add_sale_price_cart', 30, 3 );
function nutrabox_add_sale_price_cart( $price, $values, $cart_item_key ) {
	$slashed_price = $values['data']->get_price_html();	
	$is_on_sale = $values['data']->is_on_sale();
	if ( $is_on_sale ) {
 		$price = $slashed_price;
	}
	return $price;
}